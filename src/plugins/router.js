import VueRouter from "vue-router";
import Vue from "vue";

import Home from "../views/Home.vue";
import studentEducation from "../components/student/Education.vue";
import studentLager from "../components/student/lager.vue";
import studentHoger from "../components/student/hoger.vue";
import studentForm from "../components/student/form.vue";
import studentQue from "../components/student/que.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Home },
    { path: "/student/education", name: "studentEducation", component: studentEducation },
    { path: "/student/lager", name: "studentLager", component: studentLager },
    { path: "/student/hoger", name: "studentHoger", component: studentHoger },
    { path: "/student/hoger", component: studentHoger },
    { path: "/student/form/:vak", name: "studentForm", component: studentForm },
    { path: "/student/que", name: "studentQue", component: studentQue }
  ]
});
